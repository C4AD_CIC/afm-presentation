# Atomic Force Microscopy Introduction

This slide show was first (version 1.0.0) used for [GRAD NET](http://www.open.ac.uk/students/research/sepnet), part of the South East physics Network. 

It was meant for under graduates starting or Atomic Force Microscopy (AFM) and is not meant to show any data. Just an introduction on what it implies to go down the particular rabit hole of science that is AFM. 


I was later invited to give a talk at the [Richmond Scientific Society](http://www.rss.btck.co.uk/) so the focus slightly changed but the concept is the same, make it so that no prior knowledge is  needed to understand the talk. 

Licence is CC-BY-SA by Andres Muniz-Piniella and C4AD CIC. 


To work on this variation you will probably need [Inkscape](https://inkscape.org/) with the latest [Sozi](http://sozi.baierouge.fr/pages/10-about.html) installed. 



